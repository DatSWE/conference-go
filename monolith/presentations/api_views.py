from django.http import JsonResponse
import json
from .models import Presentation
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from events.models import Conference


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties =[
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    #     "status": presentation.status.name,
    #     "conference": {
    #         "name": presentation.conference.name,
    #         "href": presentation.get_api_url(),

    ]

class PresentationListEncoder(ModelEncoder):
    model=Presentation
    properties =[
        "title",
        "status",
    ]

    def get_extra_data(self, o):
        return {"status":o.status.name}

@require_http_methods(["GET","POST"])
def api_list_presentations(request, conference_id):
    
    if request.method =="GET":

        presentation = Presentation.objects.filter(conference = conference_id)
        return JsonResponse({"presentation":presentation},encoder =PresentationListEncoder)
    
    else:
        content=json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        
        except Conference.DoesNotExist:
            return JsonResponse({"message":"Invalid Conference"},status = 400,)
        
        presentation = Presentation.create(**content)
        return JsonResponse(presentation,encoder =PresentationListEncoder,safe=False)
    # presentations = [
    #     {
    #         "title": p.title,
    #         "status": p.status.name,
    #         "href": p.get_api_url(),
    #     }
    #     for p in Presentation.objects.filter(conference=conference_id)
    # ]
    # return JsonResponse({"presentations": presentations})

@require_http_methods(["GET","DELETE","PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":

        presentation = Presentation.objects.get(id=id)
        return JsonResponse(presentation, encoder = PresentationDetailEncoder,safe=False)
    
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted":count > 0})
    
    else:
         # copied from create
        content = json.loads(request.body)
        try:
        # new code
            if "presentation" in content:
                presentation = Presentation.objects.get(id=content["conference"])
                content["presentation"] = presentation
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid PresentationX=D"},
                status=400,
            )

    # new code
        Presentation.objects.filter(id=id).update(**content)

    # copied from get detail
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )

   
    # presentation = Presentation.objects.get(id=id)

    # return JsonResponse({
    #     "presenter_name": presentation.presenter_name,
    #     "company_name": presentation.company_name,
    #     "presenter_email":presentation.presenter_email,
    #     "title": presentation.title,
    #     "synopsis": presentation.synopsis,
    #     "created": presentation.created,
    #     "status": presentation.status.name,
    #     "conference": {
    #         "name": presentation.conference.name,
    #         "href": presentation.get_api_url(),
    #     }
    # })
