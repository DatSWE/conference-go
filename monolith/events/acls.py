import requests
import json
from .keys import *

def get_location_photo(city,state):
    try:
        url = "https://api.pexels.com/v1/search"
        params={
            "query":city + " " + state,
            "per_page": 1,
        }

        headers = {
            "Authorization":PEXELS_API_KEYS,
        }
        res = requests.get(url,params=params,headers=headers)
        unencoded = json.loads(res.content)
        url = unencoded["photos"][0]["url"]

        return {"picture_url": url}
    except (KeyError,IndexError):
        return {"picture_url": None}
    
def get_weather(city_name,state_name):
    
    url =f"http://api.openweathermap.org/geo/1.0/direct?q={city_name},{state_name},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
    response =requests.get(url)
    lat = json.loads(response.content)[0]["lat"]
    lon = json.loads(response.content)[0]["lon"]

    url_2=f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url_2)
    response_decode = json.loads(response.content)

    description = response_decode["weather"][0]["description"]
    temp = response_decode["main"]["temp"]
    weather = {"weather":description,"temp":temp}
    
    return weather




    
