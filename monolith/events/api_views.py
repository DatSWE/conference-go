from django.http import JsonResponse

from .models import Conference, Location,State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .acls import get_location_photo,get_weather

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties =[
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "state",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state":o.state.abbreviation}

class LocationListEncoder(ModelEncoder):
    model = Location
    properties =["name"]



class ConferenceDetailEncoder(ModelEncoder):
    model= Conference
    properties =[
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
        
    ]
    encoders ={"location":LocationListEncoder()}

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties =[
        "name"
    ]

@require_http_methods(["GET","POST"])
def api_list_conferences(request):
    
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse({"conferences":conferences},encoder = ConferenceListEncoder)
    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] =location
        except Location.DoesNotExist:
            return JsonResponse({"message":"Invalid location id"},status = 400)
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    # response = []
    # conferences = Conference.objects.all()
    # for conference in conferences:
    #     response.append(
    #         {
    #             "name": conference.name,
    #             "href": conference.get_api_url(),
    #         }
    #     )
    # return JsonResponse({"conferences": response})

@require_http_methods(["DELETE","GET","PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        # if conference.weather == None:
        final_weather = get_weather(conference.location.city,conference.location.state.abbreviation)
        final_weather["conference"] = conference
        final= final_weather
       
            # conference.weather= final_weather
            # conference.save()
        
        
        

        return JsonResponse(final,encoder=ConferenceDetailEncoder,safe = False)
    

    
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted":count > 0})
    
    else:
        try:
            content = json.loads(request.body)
            Conference.objects.filter(id=id).update(**content)
            conference = Conference.objects.get(id=id)
        except Exception:
            return JsonResponse({"message": "wrong"})
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


    # conference = Conference.objects.get(id=id)
    # return JsonResponse(
    #     {
    #         "name": conference.name,
    #         "starts": conference.starts,
    #         "ends": conference.ends,
    #         "description": conference.description,
    #         "created": conference.created,
    #         "updated": conference.updated,
    #         "max_presentations": conference.max_presentations,
    #         "max_attendees": conference.max_attendees,
    #         "location": {
    #             "name": conference.location.name,
    #             "href": conference.location.get_api_url(),
    #         },
    #     }
    # )

@require_http_methods(["GET","POST"])
def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
    if request.method =="GET":

        locations = Location.objects.all()
        return JsonResponse({"locations":locations},encoder =LocationListEncoder)
    else:
        content=json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
            # location = Location.objects.create(**content)
            # return JsonResponse(location,encoder =LocationDetailEncoder,safe=False)
        except State.DoesNotExist:
            return JsonResponse({"message":"Invalid state abbreviation"},status = 400,)
        
        picture_url = get_location_photo(content["city"],content["state"].abbreviation)
        content.update(picture_url)

        location = Location.objects.create(**content)
        return JsonResponse(location,encoder =LocationDetailEncoder,safe=False)
        
    # locations = [{
    #     "name":locations.name,
    #     "href":locations.get_api_url(),}
    #     for locations in Location.objects.all()]

    # return JsonResponse({"location":locations})
#Same thing just did it with list comprehension



    # response = []
    # locations = Location.objects.all()
    # for location in locations:
    #     response.append({
    #         "name":location.name,
    #         "href":location.get_api_url()
    #     })
    
    # return JsonResponse({"list_location":response})

@require_http_methods(["DELETE","GET","PUT"])
def api_show_location(request, id):
    
    
    if request.method == "GET":
        location = Location.objects.get(id=id)
        if location.picture_url is None:
            picture_url = get_location_photo(
                location.city, location.state.abbreviation
                )
            location.picture_url = picture_url["picture_url"]
            location.save()
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted":count > 0})
    
    else:
         
        content = json.loads(request.body)
        try:
        
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

    
        Location.objects.filter(id=id).update(**content)

    
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    # location = Location.objects.get(id=id)
    # return JsonResponse({
        
    #     "name": location.name,
    #     "city": location.city,
    #     "room_count": location.room_count,
    #     "created": location.created,
    #     "updated": location.updated,
    #     "state": location.state.abbreviation
    
    # })
