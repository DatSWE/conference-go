# Generated by Django 4.2.2 on 2023-06-25 07:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_location_picture_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='conference',
            name='weather',
            field=models.TextField(null=True),
        ),
    ]
