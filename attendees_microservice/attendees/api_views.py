from django.http import JsonResponse

from .models import Attendee,ConferenceVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json


class ConferenceVODetailEncoder(ModelEncoder):
    model= ConferenceVO
    properties =["name","import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties =["name","email"]

class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties =["name","email","company_name","created"]

@require_http_methods(["GET","POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees= Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse({"attendees":attendees},encoder = AttendeeListEncoder)
    else:
        content = json.loads(request.body)
        try:
            conference_href = f'/api/conferences/{conference_vo_id}/'
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] =conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse({"message":"Invalid conderence id"},status =400,)
        
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,encoder = AttendeeDetailEncoder,safe=False,
        )
    # list_attendees =[]
    # attendees = Attendee.objects.get(id=conference_id)
    # list_attendees.append(
    #     {"name":attendees.name,
    #     "href":attendees.get_api_url()}
    #     )
    # return JsonResponse({"list_attendees":list_attendees})
#Same thing just did without list comprehension instead
    
    # list_attendees = [
    #     {
    #         "name": attendee.name,
    #         "href":attendee.get_api_url(),
    #     }
    #     for attendee in Attendee.objects.filter(id=conference_id)
    # ]
    
    # return JsonResponse({"list_attendees":list_attendees})

@require_http_methods(["DELETE","GET","PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":

        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,encoder=AttendeeDetailEncoder,safe=False,
        )
    
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted":count > 0})
    
    else:
         # copied from create
        content = json.loads(request.body)
        try:
        # new code
            if "conference" in content:
                conference = Conference.objects.get(name=content["conference"])
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conferenceX=D"},
                status=400,
            )

    # new code
        Attendee.objects.filter(id=id).update(**content)

    # copied from get detail
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )







    # attendee = Attendee.objects.get(id=id)
    # return JsonResponse({
    #     "email": attendee.email,
    #     "name": attendee.name,
    #     "company_name":attendee.company_name,
    #     "created": attendee.created,
    #     "conference": {
    #         "name": attendee.conference.name,
    #         "href": attendee.conference.get_api_url(),
    #     }
    # })
   
